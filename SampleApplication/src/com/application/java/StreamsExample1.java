package com.application.java;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsExample1 {

	/***
	 * 
	 * https://stackify.com/streams-guide-java-8/
	 */
	public class Employee {
		
		private int empId;
		
		private String empName;
		
		private int empsalary;

		public int getEmpId() {
			return empId;
		}

		public void setEmpId(int empId) {
			this.empId = empId;
		}

		public String getEmpName() {
			return empName;
		}

		public void setEmpName(String empName) {
			this.empName = empName;
		}

		public int getEmpsalary() {
			return empsalary;
		}

		public void setEmpsalary(int empsalary) {
			this.empsalary = empsalary;
		}

		public Employee(int empId, String empName, int empsalary) {
			super();
			this.empId = empId;
			this.empName = empName;
			this.empsalary = empsalary;
		}
	}
	
	public static void main(String[] args) {

		StreamsExample1 streamExample = new StreamsExample1();
		
		// Obtaining Streams from existing Array
		
		  Employee[] arrayOfEmps = {
				  streamExample.new Employee(1,"Jeff Stalin", 12000),
				  streamExample.new Employee(2,"Jessy Caty",34000),
				  streamExample.new Employee(3,"Catherin Berk", 23000)
		};
		Stream<Employee> streamOfEmployee = Stream.of(arrayOfEmps);  
		System.out.println("Stream of employees from array = " + streamOfEmployee.count());
		
		// Obtaining Streams from existing List
		
		List<Employee> listOfEmployee = Arrays.asList(arrayOfEmps);
		System.out.println("Stream of employees from list = " + listOfEmployee.stream().count());
		
		// Creating Streams from individual objects of array
		System.out.println("Stream of employees formed from individual object from array = " + Stream.of(arrayOfEmps[0],arrayOfEmps[1],arrayOfEmps[2]).count());
		
		// Creating Streams using StreamBuilder
		
		Stream.Builder<Employee> empStreamBuilder = Stream.builder();
		empStreamBuilder.accept(arrayOfEmps[0]);
		empStreamBuilder.accept(arrayOfEmps[1]);
		empStreamBuilder.accept(arrayOfEmps[2]);
		
		Stream<Employee> empStream = empStreamBuilder.build();
		System.out.println("Stream of employees using Stream.Builder = " + empStream.count());
		
		//Finding the highest earning employees without streams
		List<Employee> empList = Arrays.asList(arrayOfEmps);
		empList.sort((s1,s2) -> s2.getEmpsalary() - s1.getEmpsalary());
		for(int i = 0;i< 3;i++)
		{
			Employee emp = empList.get(i);
			System.out.println(emp.getEmpName());
		}
		
		//Using Streams
		System.out.println("Using streams to find first 3 highest earning employees");
		empList.stream()
			.sorted(Comparator.comparingInt(Employee::getEmpsalary).reversed())
			.limit(3)
			.map(Employee::getEmpName)
			.forEach(System.out::println);
	}

}
