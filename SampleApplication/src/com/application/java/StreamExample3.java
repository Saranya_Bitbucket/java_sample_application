package com.application.java;

import java.util.Arrays;
import java.util.HashSet;
import java.util.IntSummaryStatistics;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


// Java 8 Streams API - https://www.youtube.com/watch?v=N3gQdIn90CI
public class StreamExample3 {

	//Find minimum number from an array
	public static void main(String[] args) {

		// without using streams
		int[] numbers = {};
		if (numbers.length != 0) {
			int min = numbers[0];
			for (int i = 0; i < numbers.length; i++) {
				if (numbers[i] < min) {
					min = numbers[i];
				}
			}
			System.out.println("Minimum Number is " + min);
		}
		
		//using streams
		int[] numbersArray = {30,40};
		OptionalInt minNumber = IntStream.of(numbersArray).min();
		//Prints only if minimum value is there
		minNumber.ifPresent(System.out::println);
		minNumber.ifPresentOrElse(value -> System.out.println("Minimum value is " + value), () -> System.out.println("No minimum value is present"));
		
		// throws NoSuchElementException if value is null
		int minNumber1 = IntStream.of(numbersArray).min().getAsInt();
		System.out.println("Minimum number 1 " + minNumber1);
		
		int[] numbersArray1 = {70,20,40,30,60};
		IntStream.of(numbersArray1).min().ifPresent(System.out::println);
		IntStream.of(numbersArray1).max().ifPresent(System.out::println);
		IntStream.of(numbersArray1).average().ifPresent(System.out::println);
		IntStream.of(numbersArray1).count();
		IntStream.of(numbersArray1).sum();
		
		//Using InSummaryStatistics
		IntSummaryStatistics statistics = IntStream.of(numbersArray1).summaryStatistics();
		statistics.getMax();
		statistics.getMin();
		statistics.getAverage();
		statistics.getCount();
		statistics.getSum();
		
		//Find 3 distinct smallest numbers from an array of int
		int[] numbersArray2 = {4,5,1,0,2,5,4};
		int[] clone = Arrays.copyOf(numbersArray2, numbersArray2.length);
		Arrays.sort(clone);
		HashSet<Integer> set = new HashSet<>(); 
		for (int i = 0; i < 3; i++ )
		{
			if(!set.contains(clone[i]))
			{
				set.add(clone[i]);
				System.out.println(clone[i]);
			}
		}
		
		//Find 3 distinct smallest numbers from an array of int  - Streams
		IntStream.of(numbersArray2).distinct().sorted().limit(3).forEach(System.out::println);
		
		int sum = IntStream.of(numbersArray2).distinct().sorted().limit(3).sum();
		System.out.println("Sum of 3 distict small numbers "+ sum);
		
		//IntStream - Creating Arrays with numbers
		System.out.println("Arrays with numbersArray");
		IntStream.of(numbersArray2).forEach(System.out::println);
		System.out.println("Range 0 to 4");
		IntStream.range(0, 5).forEach(System.out::println);
		System.out.println("Range 1 to 5");
		IntStream.rangeClosed(1, 5).forEach(System.out::println);
		//System.out.println("From Supplier");
		//Supplier<Integer> supplier;
		//IntStream.generate(supplier.get());
		
		//IntStream proccessing
		System.out.println("Distinct");
		IntStream.of(numbersArray2).distinct().forEach(System.out::println);
		System.out.println("Sorted");
		IntStream.of(numbersArray2).sorted().forEach(System.out::println);
		System.out.println("Limit to 3");
		IntStream.of(numbersArray2).limit(3).forEach(System.out::println);
		System.out.println("Find first");
		int firstElement = IntStream.of(numbersArray2).findFirst().getAsInt();
		System.out.println(firstElement);
		System.out.println("Skip 3");
		IntStream.of(numbersArray2).skip(3).forEach(System.out::println);
		System.out.println("Even numbers");
		IntStream.of(numbersArray2).filter(num -> num % 2 == 0).forEach(System.out::println);
		System.out.println("Doubling each number");
		IntStream.of(numbersArray2).map(num -> num * 2).forEach(System.out::println);
		System.out.println("Boxing to Integers");
		IntStream.of(numbersArray2).boxed().forEach(System.out::println);
		
		//IntStream consume
		IntStream.of(numbersArray2).anyMatch(num -> num % 2 == 1);
		IntStream.of(numbersArray2).allMatch(num -> num % 2 == 1);
		
		System.out.println("Range 1 to 5 using range");
		IntStream.range(1, 6).forEach(System.out::println);
		IntStream.range(1, 6).toArray();
		IntStream.range(1, 6).boxed().collect(Collectors.toList());
		
	}
}
