package com.application.java;

public class My_Class_Example_4 {

	public void displayMessage( Message_Example_4 message )
	{
		System.out.println(message.greet() + "This is an example of anonymous inner class as argument");
	}
	
	public static void main ( String args[] )
	{
		My_Class_Example_4 myClass = new My_Class_Example_4();
		myClass.displayMessage(new Message_Example_4() {
			
			@Override
			public String greet() {
				return "Hello.... ";
			}
		});
	}
}
