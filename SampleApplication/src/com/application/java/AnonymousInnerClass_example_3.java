package com.application.java;

/***
 * Anonymous Inner Class
 * @author SARANYA
 *
 */

public class AnonymousInnerClass_example_3 {

	public static void main(String[] args) {

		AnonymousInner_Example_3 inner = new AnonymousInner_Example_3() {
			
			@Override
			public void myMethod() {
				System.out.println("This is anonymous inner class");
			}
		};
		
		inner.myMethod();
		
	}

}
