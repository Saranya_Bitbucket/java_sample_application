package com.application.java;

public class PrimitiveDataTypeExample {

	public static void main(String[] args) {
		
		/*
		 * int, long, char literals 
		 * short, byte no literals
		 * 
		 * https://howtodoinjava.com/java/basics/primitive-data-types-in-java/
		 * https://www.ntu.edu.sg/home/ehchua/programming/java/DataRepresentation.html#zz-5.3
		 * */

		//int is 32 bit signed ..-2^31 to 2^31-1 ie -2147483648 to 2147483647
		//int num = 2147483648; // value ouside the range hence compilation error
		int num1 = 10;
		long num2 = 25L;
		// num1 = num2;  //compilation-error, should cast to int
		num1 = (int)num2; //while doing this if the value of num2 is outside the range of int , then there will be data loss eg: below
		System.out.println("Value of num1 = " + num1);
		
		//long is 64 bit signed..-2^63 to 2^63-1
		int num4 = 6;
		long num5 = 2147483648L;
		num4=(int)num5;
		System.out.println("Value of num4 = " + num4); // output: -2147483648
		
		num4 = 6;
		num5 = 2147483647L;
		num4 = (int)num5;
		System.out.println("Value of num4 = " + num4); // output: 2147483647
		
		// byte is 8 bit signed..-2^7 to 2^7-1 ie -128 to 127
		//byte b1 = 150; // if value of byte is outside the range then compilation-error
		byte b2 = (byte)150; // adding cast solves above error, but possible loss of precision output: -106
		System.out.println("Value of b2 = " +  b2);
		byte b3 = 125;
		int num6 = 150;
		b3 = (byte) num6;
		System.out.println("Value of b3 = " + b3);
		
		//short is 16 bit signed ..-2^15 to 2^15-1 ie -32768 to 32767
		short s1 = 32234;
		short s2 = -32234;
		System.out.println("Value of s1 = " + s1);
		System.out.println("Value of s2 = " + s2);
		
		//char is 16 bit unsigned..2^16 ie 65536
		// char literal
		char c1 = 'A';
		System.out.println("Value of c1 = " + c1);
		//char escape sequence - next line
		char c2 = '\n' ;
		System.out.println("Value of c2 " + c2);
		//char escape sequence - carriage return
		char c3 = '\r' ;
		System.out.println("Value of c3 " + c3);
		//char escape sequence - carriage return
		char c4 = '\f';
		System.out.println("Value of c4 " + c4);
		//char escape sequence - tab
		char c5 = '\t' ;
		System.out.println("Value of c5 " + c5);
		//char escape sequence - double quotes
		char c6 =  '\"' ;
		System.out.println("Value of c6 " + c6);
		//char escape sequence - single quotes
		char c7 =  '\'' ;
		System.out.println("Value of c7 " + c7);
		//char escape sequence - backslash
		char c8 =  '\\' ;
		System.out.println("Value of c8 " + c8);
		//char escape sequence - backspace
		char c9 = '\b' ;
		System.out.println("Value of c9 " + c9);
		
		//float data type..32 single precision
		float f1 = 12.3456f;
		System.out.println("Value of f1 = " + f1);
		
		//double data type..64 bit double precision
		double d1 = 234.5678;
		double d2 = 234.;
		double d3 = 34.56d;
		double d4 = 897.089D;
		System.out.println("Value of d1 = " + d1);
		
		//boolean..no size specified upto JVM..stored as byte
		boolean b1 = true;
		System.out.println("Value of b1 = " + b1);
	}

}
