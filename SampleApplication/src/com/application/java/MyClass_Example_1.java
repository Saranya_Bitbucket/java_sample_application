package com.application.java;

public class MyClass_Example_1 {

	private int number = 10;
	//public inner class
	public class OuterDemo_Class_Example_1 {

		public int num = 5;

		// private inner class
		private class InnerDemo_Class_Example_1 {
			public void print() {
				System.out.println("This is inner class");
			}
		}
		
		// method that of public inner class that calls the private inner class
		public void display_Inner() {
			InnerDemo_Class_Example_1 innerClass = new InnerDemo_Class_Example_1();
			innerClass.print();
		}
		
		public void getValue() {
			System.out.println("Value of number = " + number);
		}
		
	}
	public static void main(String[] args) {
		
		MyClass_Example_1 myClass = new MyClass_Example_1();
		
		// accessing public inner class with instance of outer class
		OuterDemo_Class_Example_1 outerClass = myClass.new OuterDemo_Class_Example_1();
		outerClass.display_Inner();
		
		//using the public inner class method to access the value of priavte member of the outer class
		outerClass.getValue();
	}

}
