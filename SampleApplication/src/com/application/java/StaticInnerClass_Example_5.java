package com.application.java;

public class StaticInnerClass_Example_5 {

	public static class InnerClass_Example_5{
		public void print() {
			System.out.println("This is Static Inner Class");
		}
	}
	
	public static void main(String[] args) {

		StaticInnerClass_Example_5.InnerClass_Example_5 innerClass= new StaticInnerClass_Example_5.InnerClass_Example_5();
		innerClass.print();
		
	}

}
