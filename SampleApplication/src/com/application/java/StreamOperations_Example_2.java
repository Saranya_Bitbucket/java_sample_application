package com.application.java;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

import com.application.java.StreamsExample1.Employee;

public class StreamOperations_Example_2 {

public class Employee {
		
		private int empId;
		
		private String empName;
		
		private float empsalary;

		public int getEmpId() {
			return empId;
		}

		public void setEmpId(int empId) {
			this.empId = empId;
		}

		public String getEmpName() {
			return empName;
		}

		public void setEmpName(String empName) {
			this.empName = empName;
		}

		public float getEmpsalary() {
			return empsalary;
		}

		public void setEmpsalary(float empsalary) {
			this.empsalary = empsalary;
		}

		public Employee(int empId, String empName, float empsalary) {
			super();
			this.empId = empId;
			this.empName = empName;
			this.empsalary = empsalary;
		}
		
		public void incrementBy10(float salary)
		{
			this.empsalary = this.empsalary + 10;
			
		}
	}	
	
	public static void main(String[] args) {
		
		StreamOperations_Example_2 streamExample = new StreamOperations_Example_2();
		
		
		  Employee[] arrayOfEmps = {
				  streamExample.new Employee(1,"Jeff Stalin", 12000),
				  streamExample.new Employee(2,"Jessy Caty",34000),
				  streamExample.new Employee(3,"Catherin Berk", 23000)
		};
		  List<Employee> listOfEmployee = Arrays.asList(arrayOfEmps);
		 listOfEmployee.stream().forEach(e -> e.incrementBy10(10));
		 
		 PrintStream printStream = new PrintStream(System.out);
		 printStream.println(Arrays.asList(listOfEmployee));
		 
	}

}
