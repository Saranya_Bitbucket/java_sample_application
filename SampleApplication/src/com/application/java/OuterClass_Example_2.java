package com.application.java;

/***
 * Method local inner class
 * @author SARANYA
 *
 */
public class OuterClass_Example_2 {

	public int num = 5;
	
	public void display_method_local_inner_class() {
		int number = 10;
		
		 class Inner_method_local_class{
			public void display_value_number() {
				System.out.println("Value of number = " + number);
			}
		}
		 
		 Inner_method_local_class innerClass = new Inner_method_local_class();
		 innerClass.display_value_number();
	}
	
	public static void main(String[] args) {

		OuterClass_Example_2 outerClass = new OuterClass_Example_2();
		outerClass.display_method_local_inner_class();
		
	}

}
