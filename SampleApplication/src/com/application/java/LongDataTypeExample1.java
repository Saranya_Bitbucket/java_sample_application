/**
 * 
 */
package com.application.java;

import java.util.ArrayList;
import java.util.List;

/**
 * @author SARANYA
 *
 */
public class LongDataTypeExample1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/*
		 * -XX:AutoBoxCacheMax=1000
		 * -Djava.lang.Long.LongCache.high=256
		 * 
		 * https://javapapers.com/java/java-integer-cache/
		 * https://javarevisited.blogspot.com/2010/10/what-is-problem-while-using-in.html
		 * 
		*/
		
		Long number1 = 344431L;
		Long number2 = 344431L;
		
		if (number1 == number2)
		{
			System.out.println("number1==number2");
		}
		else
		{
			System.out.println("number1!=number2");
		}
		
		Long number3 = 127L;
		Long number4 = 127L;
		
		if (number3 == number4)
		{
			System.out.println("number3==number4");
		}
		else
		{
			System.out.println("number3!=number4");
		}
		
		Long number5 = 128L;
		Long number6 = 128L;
		
		if(number5 == number6)
		{
			System.out.println("number5==number6");
		}
		else
		{
			System.out.println("number5!=number6");
		}

	}

}
